package machine_learning;

/**
 * This class represents another Tree structure that can solve the multi-class
 * problem using SVMs. It works the same as the TreeStructure Class, but with
 * different choices for each node. We start by separating [0,6,8,9,3] from
 * everything else and then it goes down the tree until it classifies it using
 * one class. Making such tree structure dynamic is possible, but time consuming
 * it can be improved in the future.
 * 
 * @author Ayman El Behri
 *
 */
public class OtherTreeSVM extends MultiClassSVM {
	private SVM[] binarySVMs;
	private final int marginCoef = 10000;

	/**
	 * Class constructor also creates instances of the SVM class which is used for
	 * the tree classification.
	 * 
	 * @param trainData
	 * @param testData
	 * @param trainLabels
	 * @param testLabels
	 * @param numberClasses
	 * @param epochs
	 */
	public OtherTreeSVM(double[][] trainData, double[][] testData, int[] trainLabels, int[] testLabels,
			int numberClasses, int epochs, double learnRate) {
		super(trainData, testData, trainLabels, testLabels, numberClasses, epochs, learnRate);

		binarySVMs = new SVM[9];
		raiseDimensionsClosest(this.getTestData());
		raiseDimensionsClosest(this.getTrainData());
		int[] chosenClasses1 = { 0, 6, 8, 9, 3 };
		binarySVMs[0] = new SVM(trainData, testData, trainLabels, testLabels, chosenClasses1, marginCoef,
				this.getLearningRate(), 1);

		int[] classesChosen = { 9, 2, 8, 3 };
		double[][] splitTraining = cutData(trainData, trainLabels, classesChosen);
		int[] splitTrainingLabels = cutLabels(trainLabels, classesChosen);
		double[][] splitTest = cutData(testData, testLabels, classesChosen);
		int[] splitTestLabels = cutLabels(testLabels, classesChosen);
		int[] chosenClasses2 = { 9 };
		binarySVMs[1] = new SVM(splitTraining, splitTest, splitTrainingLabels, splitTestLabels, chosenClasses2,
				marginCoef, this.getLearningRate(), 1);

		int[] classesChosen1 = { 0, 6 };
		splitTraining = cutData(trainData, trainLabels, classesChosen1);
		splitTrainingLabels = cutLabels(trainLabels, classesChosen1);
		splitTest = cutData(testData, testLabels, classesChosen1);
		splitTestLabels = cutLabels(testLabels, classesChosen1);
		int[] chosenClasses3 = { 0 };
		binarySVMs[2] = new SVM(splitTraining, splitTest, splitTrainingLabels, splitTestLabels, chosenClasses3,
				marginCoef, this.getLearningRate(), 1);

		int[] classesChosen2 = { 4, 2, 1 };
		splitTraining = cutData(trainData, trainLabels, classesChosen2);
		splitTrainingLabels = cutLabels(trainLabels, classesChosen2);
		splitTest = cutData(testData, testLabels, classesChosen2);
		splitTestLabels = cutLabels(testLabels, classesChosen2);
		int[] chosenClasses4 = { 1 };
		binarySVMs[3] = new SVM(splitTraining, splitTest, splitTrainingLabels, splitTestLabels, chosenClasses4,
				marginCoef, this.getLearningRate(), 1);

		int[] classesChosen8 = { 4, 2 };
		splitTraining = cutData(trainData, trainLabels, classesChosen8);
		splitTrainingLabels = cutLabels(trainLabels, classesChosen8);
		splitTest = cutData(testData, testLabels, classesChosen8);
		splitTestLabels = cutLabels(testLabels, classesChosen8);
		int[] chosenClasses10 = { 4 };
		binarySVMs[4] = new SVM(splitTraining, splitTest, splitTrainingLabels, splitTestLabels, chosenClasses10,
				marginCoef, this.getLearningRate(), 1);

		int[] classesChosen3 = { 9, 8, 7, 3, 5 };
		splitTraining = cutData(trainData, trainLabels, classesChosen3);
		splitTrainingLabels = cutLabels(trainLabels, classesChosen3);
		splitTest = cutData(testData, testLabels, classesChosen3);
		splitTestLabels = cutLabels(testLabels, classesChosen3);
		int[] chosenClasses5 = { 9, 5, 7 };
		binarySVMs[5] = new SVM(splitTraining, splitTest, splitTrainingLabels, splitTestLabels, chosenClasses5,
				marginCoef, this.getLearningRate(), 1);

		int[] classesChosen4 = { 9, 5, 7 };
		splitTraining = cutData(trainData, trainLabels, classesChosen4);
		splitTrainingLabels = cutLabels(trainLabels, classesChosen4);
		splitTest = cutData(testData, testLabels, classesChosen4);
		splitTestLabels = cutLabels(testLabels, classesChosen4);
		int[] chosenClasses6 = { 5 };
		binarySVMs[6] = new SVM(splitTraining, splitTest, splitTrainingLabels, splitTestLabels, chosenClasses6,
				marginCoef, this.getLearningRate(), 1);

		int[] classesChosen5 = { 9, 7 };
		splitTraining = cutData(trainData, trainLabels, classesChosen5);
		splitTrainingLabels = cutLabels(trainLabels, classesChosen5);
		splitTest = cutData(testData, testLabels, classesChosen5);
		splitTestLabels = cutLabels(testLabels, classesChosen5);
		int[] chosenClasses7 = { 9 };
		binarySVMs[7] = new SVM(splitTraining, splitTest, splitTrainingLabels, splitTestLabels, chosenClasses7,
				marginCoef, this.getLearningRate(), 1);

		int[] classesChosen6 = { 3, 8 };
		splitTraining = cutData(trainData, trainLabels, classesChosen6);
		splitTrainingLabels = cutLabels(trainLabels, classesChosen6);
		splitTest = cutData(testData, testLabels, classesChosen6);
		splitTestLabels = cutLabels(testLabels, classesChosen6);
		int[] chosenClasses8 = { 3 };
		binarySVMs[8] = new SVM(splitTraining, splitTest, splitTrainingLabels, splitTestLabels, chosenClasses8,
				marginCoef, this.getLearningRate(), 1);

	}

	/**
	 * This method trains the SVMs needed for classification
	 */
	public void train() {
		testAll();
		for (SVM svm : binarySVMs) {
			svm.train(this.getEpochs());
		}
		testAll();
	}

	/**
	 * This method classifies a data input going down a tree structure until it
	 * finds the classification.
	 * 
	 * @return integer of the classification.
	 */
	public int classify(double[] input) {
		if (binarySVMs[0].classify(input) == binarySVMs[0].getAboveIndice()) {
			if (binarySVMs[1].classify(input) == binarySVMs[1].getAboveIndice()) {
				if (binarySVMs[2].classify(input) == binarySVMs[2].getAboveIndice()) {
					return 0;
				} else {
					return 6;
				}
			} else {
				if (binarySVMs[3].classify(input) == binarySVMs[3].getAboveIndice()) {
					return 1;
				} else {
					if (binarySVMs[4].classify(input) == binarySVMs[4].getAboveIndice()) {
						return 4;
					} else {
						return 2;
					}
				}
			}
		} else {
			if (binarySVMs[5].classify(input) == binarySVMs[5].getAboveIndice()) {
				if (binarySVMs[6].classify(input) == binarySVMs[6].getAboveIndice()) {
					return 5;
				} else {
					if (binarySVMs[7].classify(input) == binarySVMs[7].getAboveIndice()) {
						return 9;
					} else {
						return 7;
					}
				}
			} else {
				if (binarySVMs[8].classify(input) == binarySVMs[8].getAboveIndice()) {
					return 3;
				} else {
					return 8;
				}
			}
		}
	}

	public void analyseIndividualSVMs() {
		for (SVM svm : binarySVMs) {
			this.printSVMDetails(svm);
		}
	}
}
