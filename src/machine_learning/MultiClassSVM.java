package machine_learning;

import java.util.Arrays;
import java.util.ArrayList;

/**
 * This is an abstract class that serves as the parent class for models that
 * solve multi-class problems using SVMs. It has all the methods needed for
 * multi class SVMs, such as Kernel Functions that raise the dimensions from 64
 * to 74, a method that tests the accuracy of the method of classification, and
 * other data manipulation methods.
 * 
 * @author Ayman El Behri
 *
 */
public abstract class MultiClassSVM {
	private double[][] trainData, testData;
	private int[] trainLabels, testLabels;
	private int numberClasses;
	private double accuracy;
	private int epochs;
	private double learningRate;

	public MultiClassSVM(double[][] trainData, double[][] testData, int[] trainLabels, int[] testLabels,
			int numberClasses, int epochs, double learningRate) {
		this.trainData = trainData;
		this.testData = testData;
		this.trainLabels = trainLabels;
		this.testLabels = testLabels;
		this.numberClasses = numberClasses;
		this.epochs = epochs;
		this.learningRate = learningRate;
	}

	/**
	 * This method finds the centroid for each class and then gets the distance of
	 * each point to each centroid, and we end up with 10 other classes.
	 */
	public void raiseDimensionsCentroid() {
		// We get the centroid of each class.
		double[][] centroids = new double[trainData[0].length][numberClasses];
		for (int index = 0; index < numberClasses; index++) {
			centroids[index] = calculateCentroid(index);
		}

		// We go through all of the data and calculate the distance to each centroid.
		// This makes the assumption that both test and train data has the same size.
		for (int index = 0; index < trainData.length; index++) {
			double[] higherDimensionValues = new double[numberClasses];
			double[] higherDimensionValuesTest = new double[numberClasses];
			double[] dataPoint = trainData[index];
			double[] testDataPoint = testData[index];
			// we calculate the distance to each centroid for both the training data and
			// test data
			for (int centroidIndex = 0; centroidIndex < numberClasses; centroidIndex++) {
				higherDimensionValues[centroidIndex] = distance(dataPoint, centroids[centroidIndex]);
				higherDimensionValuesTest[centroidIndex] = distance(testDataPoint, centroids[centroidIndex]);
			}
			// We elevate the number of dimensions using combine.
			trainData[index] = combine(dataPoint, higherDimensionValues);
			testData[index] = combine(testDataPoint, higherDimensionValuesTest);
		}
	}

	/**
	 * this method takes some data and raises its dimensions using the closest
	 * points from each class
	 * 
	 * @param data   The data we want to raise its dimensions.
	 * @param labels The labels of the data
	 */
	public void raiseDimensionsClosest(double[][] data) {
		// We store all the extra dimensions for the data.
		double[][] allExtraDimensions = new double[data.length][numberClasses];
		for (int index = 0; index < data.length; index++) {
			double[] extraDimensions = new double[numberClasses];
			for (int classValue = 0; classValue < numberClasses; classValue++) {
				// We get the closest distance to the closest point in each class.
				extraDimensions[classValue] = this.closestPointDistance(this.trainData, this.trainLabels, data[index],
						classValue);
			}
			allExtraDimensions[index] = extraDimensions;
		}
		// We then combine the extra dimensions with the original data
		for (int index = 0; index < data.length; index++) {
			data[index] = combine(data[index], allExtraDimensions[index]);
		}
	}

	/**
	 * This method takes a single input and then classifies it using some multi
	 * class method for the SVM.
	 * 
	 * @param input data input.
	 * @return an integer classification.
	 */
	public abstract int classify(double[] input);

	/**
	 * This method prints an analysis for the SVMs used for the classification.
	 */
	public abstract void analyseIndividualSVMs();

	/**
	 * This method is called to train the SVMs used for the classification.
	 */
	public abstract void train();

	/**
	 * This method tests out the accuracy of the method.
	 */
	public void testAll() {
		System.out.println("------------------------------------------------------------");
		System.out.println("Final TESTING");
		// We store the results in a confusion matrix for analysis.
		int[][] confusionMatrix = new int[numberClasses][numberClasses];
		int correctClassifications = 0;
		for (int index = 0; index < testData.length; index++) {
			// We check if a classification is equal to the label.
			double[] input = testData[index];
			int classification = classify(input);
			if (classification == testLabels[index]) {
				correctClassifications++;
			}
			confusionMatrix[testLabels[index]][classification]++;
		}
		// We print some relevant information about the data.
		accuracy = (double) correctClassifications / testData.length;
		System.out.println("The accuracy is " + accuracy);
		System.out.println("The correct classifications " + correctClassifications);
		System.out.println("The incorrect classifications " + (testData.length - correctClassifications));
		// We also print out the confusion matrix.
		System.out.println("The confusion Matrix:");
		for (int[] classConfusion : confusionMatrix) {
			System.out.println(Arrays.toString(classConfusion));
		}
		// We the check the analysis of the individual SVMs.
		analyseIndividualSVMs();
	}

	/**
	 * This method functions in the same way as the testAll() method but it tests
	 * using the training data instead.
	 */
	public void testTrainAll() {
		System.out.println("------------------------------------------------------------");
		System.out.println("Final TESTING ON TRAINING");
		int[][] confusionMatrix = new int[numberClasses][numberClasses];
		int correctClassifications = 0;
		for (int index = 0; index < trainData.length; index++) {
			double[] input = trainData[index];
			int classification = classify(input);
			if (classification == trainLabels[index]) {
				correctClassifications++;
			}
			confusionMatrix[trainLabels[index]][classification]++;
		}
		accuracy = (double) correctClassifications / trainData.length;
		System.out.println("The accuracy is " + accuracy);
		System.out.println("The correct classifications " + correctClassifications);
		System.out.println("The incorrect classifications " + (trainData.length - correctClassifications));
		System.out.println("The confusion Matrix:");
		for (int[] classConfusion : confusionMatrix) {
			System.out.println(Arrays.toString(classConfusion));
		}
		analyseIndividualSVMs();
	}

	/**
	 * This method finds the centroid of a digit (class) from the training data.
	 * 
	 * @param centroidClass the digit we want to find the centroid for.
	 * @return The centroid coordinates.
	 */
	public double[] calculateCentroid(int centroidClass) {
		double[] sum = new double[trainData[0].length];
		// We keep track of the number of points of this class to be able to average it.
		int classCount = 0;
		for (int index = 0; index < trainData.length; index++) {
			// We sum the coordinates of all the points of the same label.
			if (trainLabels[index] == centroidClass) {
				for (int sumIndex = 0; sumIndex < sum.length; sumIndex++) {
					sum[sumIndex] += trainData[index][sumIndex];
				}
				classCount++;
			}
		}
		// We then divide each of the coordinates to get the average (centroid).
		for (int index = 0; index < sum.length; index++) {
			sum[index] = sum[index] / classCount;
		}
		System.out.println("The centroid for class (" + centroidClass + ") is: " + Arrays.toString(sum)
				+ " the class count " + classCount);
		return sum;
	}

	/**
	 * This calculates the distance between two points from the data
	 * 
	 * @param firstPoint
	 * @param secondPoint
	 * @return the distance between the two points.
	 */
	public double distance(double[] firstPoint, double[] secondPoint) {
		double sum = 0;
		for (int index = 0; index < firstPoint.length; index++) {
			sum += Math.pow((firstPoint[index] - secondPoint[index]), 2);
		}
		return Math.sqrt(sum);
	}

	/**
	 * This method takes two double arrays and combines them into one array.
	 * 
	 * @param firstArray
	 * @param secondArray
	 * @return The secondArray appended to the firstArray.
	 */
	public double[] combine(double[] firstArray, double[] secondArray) {
		// We create a bigger array.
		double[] result = new double[firstArray.length + secondArray.length];
		int index = 0;
		// We copy the elements of both arrays into one bigger array.
		for (; index < firstArray.length; index++) {
			result[index] = firstArray[index];
		}
		for (; index < result.length; index++) {
			result[index] = secondArray[index - firstArray.length];
		}
		return result;
	}

	/**
	 * This method takes some data, and cuts it leaving only the chosen classes.
	 * 
	 * @param data
	 * @param labels
	 * @param classes
	 * @return The cut data.
	 */
	public double[][] cutData(double[][] data, int[] labels, int[] classes) {
		// We store the cut data temporarily in an ArrayList.
		ArrayList<double[]> slicedData = new ArrayList<double[]>();
		for (int index = 0; index < labels.length; index++) {
			// If the label of the data point was chosen we add it.
			if (linearSearch(classes, labels[index])) {
				slicedData.add(data[index]);
			}
		}
		// We then move the cut data from the ArrayList to an Array.
		double[][] result = new double[slicedData.size()][data[0].length];
		for (int index = 0; index < result.length; index++) {
			result[index] = slicedData.get(index);
		}
		return result;
	}

	/**
	 * This method cuts the labels that exist in the chosen classes. It works in the
	 * same way as the cutData.
	 * 
	 * @param labels
	 * @param classes
	 * @return an array of the labels.
	 */
	public int[] cutLabels(int[] labels, int[] classes) {
		ArrayList<Integer> cutData = new ArrayList<Integer>();
		for (int index = 0; index < labels.length; index++) {
			if (linearSearch(classes, labels[index])) {
				cutData.add(labels[index]);
			}
		}
		int[] result = new int[cutData.size()];
		for (int index = 0; index < result.length; index++) {
			result[index] = cutData.get(index);
		}
		return result;
	}

	/**
	 * This method finds the distance to the closest point in a data set with a
	 * specific label.
	 * 
	 * @param data
	 * @param labels
	 * @param point
	 * @param classValue
	 * @return distance to closest point from the data with a specific label.
	 */
	public double closestPointDistance(double[][] data, int[] labels, double[] point, int classValue) {
		// We keep track of the closest Distance.
		double closestDistance = Double.MAX_VALUE;
		for (int index = 0; index < data.length; index++) {
			// We go through the data and we ignore the labels that are not chosen.
			// We also ignore the same point which will make the closest distance for
			// the training data for the dimensions corresponding to the label of the point
			// 0.
			// We notice an improvement if we include this extra condition.
			if (classValue != labels[index] || data[index] == point)
				continue;
			double currentDistance = distance(data[index], point);
			if (currentDistance < closestDistance) {
				closestDistance = currentDistance;
			}
		}
		return closestDistance;
	}

	/**
	 * This method conducts a linear search in an array for some value.
	 * 
	 * @param array
	 * @param value
	 * @return true if the int exists in the array, return false otherwise.
	 */
	public boolean linearSearch(int[] array, int value) {
		for (int otherValue : array) {
			if (otherValue == value)
				return true;
		}
		return false;
	}

	/**
	 * This method prints some important details of an SVM.
	 * 
	 * @param svm
	 */
	public void printSVMDetails(SVM svm) {
		System.out.println("For the svm with the classes (" + Arrays.toString(svm.getClassifications())
				+ ")  the accuracy is: " + svm.getAccuracy());
		System.out.println("The SVM indice " + svm.getAboveIndice() + " And the margin coef: " + svm.getMarginCoef());
	}

	/**
	 * @return the epochs used for training this classifier.
	 */
	public int getEpochs() {
		return this.epochs;
	}

	/**
	 * @return the training data used for training this classifier.
	 */
	public double[][] getTrainData() {
		return trainData;
	}

	/**
	 * @return the test data used for testing this classifier.
	 */
	public double[][] getTestData() {
		return testData;
	}

	/**
	 * @return the training labels used for training this classifier.
	 */
	public int[] getTrainLabels() {
		return trainLabels;
	}

	/**
	 * @return the test labels used for testing this classifier.
	 */
	public int[] getTestLabels() {
		return testLabels;
	}

	/**
	 * @return the number of classes for this data.
	 */
	public int getNumberClasses() {
		return numberClasses;
	}

	/**
	 * @return The Accuracy of the classifier.
	 */
	public double getAccuracy() {
		return accuracy;
	}

	/**
	 * @return The learning rate of the SVMs used by the classifier.
	 */
	public double getLearningRate() {
		return learningRate;
	}

}
