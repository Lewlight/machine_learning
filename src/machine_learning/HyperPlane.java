package machine_learning;

/**
 * This class represents a HyperPlane in any n dimensional space. It contains
 * methods which helps check the position of a point in space relative to the
 * hyper plane.
 * 
 * @author Ayman El Behri
 *
 */
public class HyperPlane {
	// We keep these public since there is no point in encapsulating them.
	// The SVM class can modify them and retrieve them.
	public Matrix weights;
	public double bias = 0;

	/**
	 * Constructor which creates a random HyperPlane separator.
	 * 
	 * @param dimensions     the number of the dimensions the hyper plane is in.
	 * @param randomSelector is a value used to generate a random value for the
	 *                       weights.
	 */
	public HyperPlane(int dimensions, int randomSelector) {
		weights = new Matrix(dimensions, 1);
	}

	/**
	 * This constructor creates the HyperPlane from chosen weights and bias.
	 * 
	 * @param weights
	 * @param bias
	 */
	public HyperPlane(Matrix weights, double bias) {
		this.weights = weights;
		this.bias = bias;
	}

	/**
	 * This method takes some coordinates and replaces them in the Hyper Plane
	 * equation.
	 * 
	 * @param coordinates a Matrix of coordinates (a digit's values)
	 * @return The value when replacing the coordinates in the hyper plane equation.
	 */
	public double replaceCoordinates(Matrix coordinates) {

		return coordinates.dotProduct(weights).getData()[0][0] + bias;
	}

	/**
	 * This method checks if a point is above below or on the hyper plane.
	 * 
	 * @param coordinates of the point.
	 * @return 1 means the point is above the plane, -1 means the point is below the
	 *         plane, and 0 means it is on the plane.
	 */
	public int checkIfAbove(Matrix coordinates) {
		double position = replaceCoordinates(coordinates);
		if (position > 0)
			return 1;
		else if (position == 0)
			return 0;
		return -1;
	}

}
