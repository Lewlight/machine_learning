package machine_learning;

import java.util.ArrayList;

/**
 * This class represents a Batch used by the machine learning algorithms. It
 * stores the data and the labels for the batch.
 * 
 * It stores batches of data with associated labels. It partitions data into
 * batches (with input vectors and associated labels), and stores them.
 * 
 * @author Ayman El Behri
 */
public class Batch {
	private int[] labels;
	private double[][] data;
	private int size;

	public Batch(int[] labels, double[][] data) {
		this.labels = labels;
		this.data = data;
		size = data.length;
	}

	/**
	 * This is a method that takes some data and creates an array of Batch Objects
	 * with parts of the data randomly selected and partitioned into batches with
	 * their associated labels.
	 * 
	 * @param data      Data to be separated into batches (training or testing data
	 *                  for example)
	 * @param allLabels The labels of the data we want to separate.
	 * @param batchSize The batch size we choose (at least the maximum we will use).
	 * @return An array of batches which can be used for batch gradient descent.
	 *         (Example: if data size is 5 and chosen batchSize is 2) This will
	 *         return 3 batches two of them will have a size of 2, and one of size
	 *         1.
	 */
	public static Batch[] batchItUp(double[][] data, int allLabels[], int batchSize) {
		// We determine the exact number of batches we will create depending on the
		// size.
		int numberOfBatches = data.length / batchSize + 1;
		if (data.length % batchSize == 0) {
			numberOfBatches = data.length / batchSize;
		}
		Batch[] batches = new Batch[numberOfBatches];
		// We create an ArrayList of indexes for the data we will separate.
		ArrayList<Integer> dataIndexes = new ArrayList<Integer>();
		for (int index = 0; index < data.length; index++) {
			dataIndexes.add(index);
		}
		for (int batchIndex = 0; batchIndex < numberOfBatches; batchIndex++) {
			// When we reach the last batch we make sure that its size will be remaining
			// size of the data not yet assigned into a batch.
			if (batchIndex == numberOfBatches - 1) {
				batchSize = dataIndexes.size();
			}
			// We make sure that there is no batch with size 0.
			if (batchSize == 0) {
				break;
			}
			// we start populating the batch.
			double[][] batchData = new double[batchSize][];
			int[] labels = new int[batchSize];
			int dataIndex = 0;
			for (int index = 0; index < batchSize; index++) {
				// We randomly select an index (not yet chosen) before assigning it to the
				// batch.
				int randomIndex = (int) (Math.random() * dataIndexes.size());
				// We follow same logic for labels.
				labels[dataIndex] = allLabels[dataIndexes.get(randomIndex)];
				batchData[dataIndex++] = data[dataIndexes.get(randomIndex)];
				// We remove the index.
				dataIndexes.remove(randomIndex);
			}
			batches[batchIndex] = new Batch(labels, batchData);
		}
		return batches;
	}

	public int[] getLabels() {
		return labels;
	}

	public double[][] getData() {
		return data;
	}

	public int getSize() {
		return size;
	}

}
