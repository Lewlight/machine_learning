package machine_learning;


/**
 * This class solves a Multi Class problem using a tree structure.
 * The tree separates the classes {0,6,4,2} and {9, 8, 1, 7, 3, 5}, depending on which set the
 * point belongs, to we go down the if else statements until we reach an SVM that separates one class
 * from the other class(es) and which ever one it belongs to is the predicted result.
 * For this classifier a hard margin seems to give better results.
 * @author Ayman El Behri
 *
 */
public class TreeSVM extends MultiClassSVM{
	private SVM[] binarySVMs;
	private final int marginCoefficient = 10000;
	
	/**
	 * Class constructor also creates instances of the SVM class which
	 * is used for the tree classification.
	 * @param trainData
	 * @param testData
	 * @param trainLabels
	 * @param testLabels
	 * @param numberClasses
	 * @param epochs
	 */
	public TreeSVM(double[][] trainData, double[][] testData, int[] trainLabels, int[] testLabels, int numberClasses, int epochs, double learnRate) {
		super(trainData, testData, trainLabels, testLabels, numberClasses, epochs, learnRate);
		// This class has to be hard coded for simplicity.
		binarySVMs = new SVM[9];
		// We start by raising the dimensions from 64 to 74.
		raiseDimensionsClosest(getTestData());
		raiseDimensionsClosest(getTrainData());
		
		// This SVM separates the points [0,6,4,2] from every other class.
		int[] chosenClasses1 = {0,6,4,2};
		binarySVMs[0] = new SVM(trainData, testData, trainLabels, testLabels, chosenClasses1, marginCoefficient, this.getLearningRate(),1);
		
		// We create an SVM that separates [0,6] from [4,2]
		int[] classesChosen = {0,6,4,2};
		// We use cutData() and cutLabels() to keep only the vectors that belong to classesChosen.
		// This way we can train the SVM using only the data we need.
		double[][] splitTraining = cutData(trainData, trainLabels, classesChosen);
		int[] splitTrainingLabels = cutLabels(trainLabels, classesChosen);
		double[][] splitTest = cutData(testData, testLabels, classesChosen);
		int[] splitTestLabels = cutLabels(testLabels, classesChosen);
		// chosenClasses is the array of classes we want to separate from the rest.
		int[] chosenClasses2 = {0,6};
		binarySVMs[1] = new SVM(splitTraining, splitTest, splitTrainingLabels, splitTestLabels, chosenClasses2, marginCoefficient, this.getLearningRate(),1);
		
		// We use the same logic for creating each SVM.
		
		// We create an SVM that separates 0 from 6.
		int[] classesChosen1 = {0,6};
		splitTraining = cutData(trainData, trainLabels, classesChosen1);
		splitTrainingLabels = cutLabels(trainLabels, classesChosen1);
		splitTest = cutData(testData, testLabels, classesChosen1);
		splitTestLabels = cutLabels(testLabels, classesChosen1);
		int[] chosenClasses3 = {0};
		binarySVMs[2] = new SVM(splitTraining, splitTest, splitTrainingLabels, splitTestLabels, chosenClasses3, marginCoefficient, this.getLearningRate(),1);
		
		// We create an SVM that separates 4 from 2.
		int[] classesChosen2 = {4,2};
		splitTraining = cutData(trainData, trainLabels, classesChosen2);
		splitTrainingLabels = cutLabels(trainLabels, classesChosen2);
		splitTest = cutData(testData, testLabels, classesChosen2);
		splitTestLabels = cutLabels(testLabels, classesChosen2);
		int[] chosenClasses4 = {4};
		binarySVMs[3] = new SVM(splitTraining, splitTest, splitTrainingLabels, splitTestLabels, chosenClasses4, marginCoefficient, this.getLearningRate(),1);
		
		// We create an SVM that separates [9,5,1,8] from [3,5]
		int[] classesChosen3 = {9, 8, 1, 7, 3, 5};
		splitTraining = cutData(trainData, trainLabels, classesChosen3);
		splitTrainingLabels = cutLabels(trainLabels, classesChosen3);
		splitTest = cutData(testData, testLabels, classesChosen3);
		splitTestLabels = cutLabels(testLabels, classesChosen3);
		int[] chosenClasses5 = {9,5,1,8};
		binarySVMs[4] = new SVM(splitTraining, splitTest, splitTrainingLabels, splitTestLabels, chosenClasses5, marginCoefficient, this.getLearningRate(),1);
		
		// We create an SVM that separates [9,5] from [1,8]
		int[] classesChosen4 = {9,5,1,8};
		splitTraining = cutData(trainData, trainLabels, classesChosen4);
		splitTrainingLabels = cutLabels(trainLabels, classesChosen4);
		splitTest = cutData(testData, testLabels, classesChosen4);
		splitTestLabels = cutLabels(testLabels, classesChosen4);
		int[] chosenClasses6 = {9,5};
		binarySVMs[5] = new SVM(splitTraining, splitTest, splitTrainingLabels, splitTestLabels, chosenClasses6, marginCoefficient, this.getLearningRate(),1);
		
		// We create an SVM that separates 9 from 5.
		int[] classesChosen5 = {9,5};
		splitTraining = cutData(trainData, trainLabels, classesChosen5);
		splitTrainingLabels = cutLabels(trainLabels, classesChosen5);
		splitTest = cutData(testData, testLabels, classesChosen5);
		splitTestLabels = cutLabels(testLabels, classesChosen5);
		int[] chosenClasses7 = {9};
		binarySVMs[6] = new SVM(splitTraining, splitTest, splitTrainingLabels, splitTestLabels, chosenClasses7, marginCoefficient, this.getLearningRate(),1);
		
		// We create an SVM that separates 1 from 8.
		int[] classesChosen6 = {1,8};
		splitTraining = cutData(trainData, trainLabels, classesChosen6);
		splitTrainingLabels = cutLabels(trainLabels, classesChosen6);
		splitTest = cutData(testData, testLabels, classesChosen6);
		splitTestLabels = cutLabels(testLabels, classesChosen6);
		int[] chosenClasses8 = {1};
		binarySVMs[7] = new SVM(splitTraining, splitTest, splitTrainingLabels, splitTestLabels, chosenClasses8, marginCoefficient, this.getLearningRate(),1);
		
		// We create an SVM that separates 3 from 7.
		int[] classesChosen7 = {3, 7};
		splitTraining = cutData(trainData, trainLabels, classesChosen7);
		splitTrainingLabels = cutLabels(trainLabels, classesChosen7);
		splitTest = cutData(testData, testLabels, classesChosen7);
		splitTestLabels = cutLabels(testLabels, classesChosen7);
		int[] chosenClasses9 = {3};
		binarySVMs[8] = new SVM(splitTraining, splitTest, splitTrainingLabels, splitTestLabels, chosenClasses9, marginCoefficient, this.getLearningRate(),1);		
	}
	
	/**
	 * This method loops through the SVMs and trains them.
	 */
	public void train() {
		testAll();
		for(SVM svm: binarySVMs) {
			svm.train(getEpochs());
		}
		testAll();
	}
	
	/**
	 * This method classifies a vector point, following the tree structure.
	 */
	public int classify(double[] input) {
		// If the SVM[0] classifies it as 1, it means that the point
		// belongs to the set {0,6,4,2}
		if(binarySVMs[0].classify(input) == binarySVMs[0].getAboveIndice()) {
			// If the SVM[1] classifies it as 1, it means the point
			// belongs to the set {0,6}.
			if(binarySVMs[1].classify(input) == binarySVMs[1].getAboveIndice()) {
				// If the SVM[2] classifies it as 1, it means the point is 0.
				if(binarySVMs[2].classify(input) == binarySVMs[2].getAboveIndice()) {
					return 0;
				}
				// Otherwise the point is 6.
				else {
					return 6;
				}
			}
			// If the SVM[1] classifies it as -1, it means the point
			// belongs to the set {4,2}.
			else {
				// If the SVM[3] classifies it as 1, it means the point is 4.
				if(binarySVMs[3].classify(input) == binarySVMs[3].getAboveIndice()) {
					return 4;
				}
				// Otherwise the point is 6.
				else {
					return 2;
				}
			}
		}
		// If the SVM[0] classifies it as -1, it means that the point
		// belongs to the set {9, 8, 1, 7, 3, 5}.
		else {
			// If the SVM[4] classifies it as 1, it means that the point
			// belongs to the set {9, 8, 1, 5}.
			if(binarySVMs[4].classify(input) == binarySVMs[4].getAboveIndice()) {
				// If the SVM[5] classifies it as 1, it means that the point
				// belongs to the set {9, 5}.
				if(binarySVMs[5].classify(input) == binarySVMs[5].getAboveIndice()) {
					// If the SVM[6] classifies it as 1, it means the point is 9.
					if(binarySVMs[6].classify(input) == binarySVMs[6].getAboveIndice()) {
						return 9;
					}
					// Otherwise the point is 5.
					else {
						return 5;
					}
				}
				// If the SVM[5] classifies it as 1, it means that the point
				// belongs to the set {1, 8}.
				else {
					// If the SVM[7] classifies it as 1, it means the point is 1.
					if(binarySVMs[7].classify(input) == binarySVMs[7].getAboveIndice()) {
						return 1;
					}
					// Otherwise the point is 8.
					else {
						return 8;
					}
				}
			}
			// If the SVM[4] classifies it as -1, it means that the point
			// belongs to the set {3,7}.
			else {
				// If the SVM[8] classifies it as 1, it means the point is 3.
				if (binarySVMs[8].classify(input) == binarySVMs[8].getAboveIndice()) {
					return 3;
				}
				// Otherwise the point is 7.
				else {
					return 7;
				}
			}
		}
	}
	
	/**
	 * This method prints the results and information of each SVM used for the classification.
	 */
	public void analyseIndividualSVMs() {
		for(SVM svm: binarySVMs) {
			this.printSVMDetails(svm);
		}
	}
	
	
	
}
