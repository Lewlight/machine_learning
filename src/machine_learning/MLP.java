package machine_learning;

/**
 * This class is a Multi Layered Perceptron, you can specify the input size the
 * number of hidden layers, the size of each hidden layer, the number of
 * classes, and learning rate. The activation function used is a sigmoid
 * function that returns a value between 0 and 1. The highest accuracy achieved
 * on the data set in hand is 95%.
 * 
 * @author Ayman El Behri
 *
 */
public class MLP {
	private MLPLayer inputLayer;
	private MLPLayer outputLayer;
	private MLPLayer[] hiddenLayers;
	private int inputSize;
	private int numberClasses;
	private double learningRate;
	private double[][] trainData;
	private double[][] testData;
	private int[] trainLabels;
	private int[] testLabels;
	private Matrix outputLayerWeightGradiant;
	private Matrix[] hiddenLayersWeightGradiant;
	private Matrix outputLayerBiasGradiant;
	private Matrix[] hiddenLayersBiasGradiant;
	private double accuracy = 0.0;
	private double cost;

	/**
	 * The constructor creates all the layers needed for the MLP.
	 * 
	 * @param inputSize     is the size of the input data (for our case it's 64)
	 * @param numberClasses is the number of different classes we need to classify.
	 * @param hiddenSize    is an array describing the structure of the hidden
	 *                      layers, each element of the array corresponds to the
	 *                      size of a hidden layer.
	 * @param learningRate
	 * @param trainSet
	 * @param testSet
	 * @param trainLabels
	 * @param testLabels
	 */
	public MLP(int inputSize, int numberClasses, int[] hiddenSize, double learningRate, double[][] trainSet,
			double[][] testSet, int[] trainLabels, int[] testLabels) {
		this.trainData = trainSet;
		this.testData = testSet;
		this.trainLabels = trainLabels;
		this.testLabels = testLabels;

		this.learningRate = learningRate;
		this.inputSize = inputSize;
		this.numberClasses = numberClasses;

		inputLayer = new MLPLayer(inputSize);
		hiddenLayers = new MLPLayer[hiddenSize.length];
		// We instantiate the input, hidden, and output layers.
		hiddenLayers[0] = new MLPLayer(inputSize, hiddenSize[0]);
		for (int index = 1; index < hiddenSize.length; index++) {
			hiddenLayers[index] = new MLPLayer(hiddenSize[index - 1], hiddenSize[index]);
		}
		outputLayer = new MLPLayer(hiddenSize[hiddenSize.length - 1], numberClasses);
	}

	/**
	 * This method trains using mini-batch gradient descent.
	 * 
	 * @param batchSize the size of the batches we want to use.
	 * @param epochs
	 */
	public void trainBatches(int batchSize, int epochs) {
		testAll();
		// We first create the batches.
		Batch[] batches = Batch.batchItUp(trainData, trainLabels, batchSize);
		for (int epoch = 0; epoch < epochs; epoch++) {
			for (int index = 0; index < batches.length; index++) {
				Batch batch = batches[index];
				// And train using each batch.
				trainBatch(batch);
			}
			System.out.println("The cost for last batch " + " is " + cost / batches[0].getSize());
		}
		testAll();
	}

	/**
	 * This method trains using a single batch.
	 * 
	 * @param batch
	 */
	public void trainBatch(Batch batch) {
		// We reset the cost to 0, so we can calculate the cost of this batch.
		cost = 0;
		// We reset the gradients variables for both weights and biases.
		hiddenLayersWeightGradiant = new Matrix[hiddenLayers.length];
		outputLayerWeightGradiant = new Matrix(outputLayer.activation.getData().length,
				hiddenLayers[hiddenLayers.length - 1].activation.getData().length);
		hiddenLayersBiasGradiant = new Matrix[hiddenLayers.length];
		outputLayerBiasGradiant = new Matrix(outputLayer.activation.getData().length, 1);
		// We then train using this batch.
		for (int index = 0; index < batch.getSize(); index++) {
			double[] input = batch.getData()[index];
			int label = batch.getLabels()[index];
			train(input, label);
		}
		// We update the weights and bias using the gradients that were found while
		// training.
		updateWeights(batch.getSize());
	}

	/**
	 * This method trains using all of the training data, which is slower than mini
	 * batch gradient.
	 * 
	 * @param epochs
	 */
	public void trainAll(int epochs) {
		testAll();
		for (int iter = 0; iter < epochs; iter++) {
			// We reset the cost and the gradients.
			cost = 0;
			hiddenLayersWeightGradiant = new Matrix[hiddenLayers.length];
			outputLayerWeightGradiant = new Matrix(outputLayer.activation.getData().length,
					hiddenLayers[hiddenLayers.length - 1].activation.getData().length);
			hiddenLayersBiasGradiant = new Matrix[hiddenLayers.length];
			outputLayerBiasGradiant = new Matrix(outputLayer.activation.getData().length, 1);
			for (int index = 0; index < trainData.length; index++) {
				// We train using one data point, finding the gradient step.
				double[] input = trainData[index];
				int label = trainLabels[index];
				train(input, label);
			}
			// And we update the weight after each iteration.
			System.out.println("The cost is " + cost / trainData.length);
			updateWeights(trainData.length);
		}
		testAll();
	}

	/**
	 * This method finds the accuracy of the MLP on the testing data.
	 */
	public double testAll() {
		int[] correctClassAccuracy = new int[numberClasses];
		int[] incorrectClassAccuracy = new int[numberClasses];
		int correctClassifications = 0;
		for (int index = 0; index < testData.length; index++) {
			double[] input = testData[index];
			int label = testLabels[index];
			int classification = classify(input);
			if (label == classification) {
				correctClassifications++;
				correctClassAccuracy[label] = correctClassAccuracy[label] + 1;
			} else {
				incorrectClassAccuracy[label] = incorrectClassAccuracy[label] + 1;
			}
		}
		accuracy = (double) correctClassifications / testData.length;
		System.out.println("The accuracy is " + accuracy);
		System.out.println("The correct classifications " + correctClassifications);
		System.out.println("The incorrect classifications " + (testData.length - correctClassifications));
		for (int index = 0; index < numberClasses; index++) {
			System.out.println("The accuracy of " + index + " -> correct: " + correctClassAccuracy[index]
					+ "// incorrect: " + incorrectClassAccuracy[index]);
		}
		return accuracy;
	}

	/**
	 * This method finds the accuracy of the MLP on the training data.
	 */
	public double testTrainAll() {
		int correctClassifications = 0;
		for (int index = 0; index < trainData.length; index++) {
			double[] input = trainData[index];
			int label = trainLabels[index];
			if (label == classify(input)) {
				correctClassifications++;
			}
		}
		double otherAccuracy = (double) correctClassifications / trainData.length;
		System.out.println("The accuracy is " + otherAccuracy);
		System.out.println("The correct classifications " + correctClassifications);
		System.out.println("The incorrect classifications " + (trainData.length - correctClassifications));
		return otherAccuracy;
	}

	/**
	 * This method sends the data through the MLP, calculates its cost, and then
	 * calculates and saves the gradient values.
	 * 
	 * @param input          a single input.
	 * @param classification the correct label of the input.
	 */
	public void train(double[] input, int classification) {
		// We have some validation of the input.
		if (input.length != inputSize) {
			System.out.println("The input size is unacceptable " + inputSize);
		} else {
			// We send the data through the MLP.
			feedForward(input);
			// We sum up the cost, so we can average it.
			cost += calculateCost(classification);
			// We save the gradient.
			backPropagateSave(classification);
		}
	}

	/**
	 * This method is used to classify an input using arg max on the output layer.
	 * 
	 * @param input
	 * @return
	 */
	public int classify(double[] input) {
		if (input.length != inputSize) {
			System.out.println("The input size is unacceptable " + inputSize);
			System.exit(1);
		} else {
			// We first send the data through the MLP.
			feedForward(input);
			int highestIndex = 0;
			double highestValue = 0;
			// We then loop through the output layer to find the perceptron with the highest
			// activation.
			for (int index = 0; index < numberClasses; index++) {
				double[] row = outputLayer.activation.getData()[index];
				if (row[0] >= highestValue) {
					highestIndex = index;
					highestValue = row[0];
				}
			}
			// The highest activation is the predicted class.
			return highestIndex;
		}
		// We return -1 in case of an error.
		return -1;
	}

	/**
	 * This method passes the data through each layer and gets their activation.
	 * 
	 * @param input the data input.
	 */
	public void feedForward(double[] input) {
		if (input.length != inputSize) {
			System.out.println("The input size is unacceptable " + inputSize);
		} else {
			// We set the activation of the input layer to be the same as the actual data
			// point.
			// The application of sigmoid can be commented out to check the its effect.
			inputLayer.activation = new Matrix(input);
			// We pass the input layer to the first hidden layer.
			hiddenLayers[0].feedPrevious(inputLayer);
			hiddenLayers[0].activation.applySigmoid();
			for (int index = 1; index < hiddenLayers.length; index++) {
				// We then pass each hidden layer's activation to the next hidden layer.
				hiddenLayers[index].feedPrevious(hiddenLayers[index - 1]);
				hiddenLayers[index].activation.applySigmoid();
			}
			outputLayer.feedPrevious(hiddenLayers[hiddenLayers.length - 1]);
		}
	}

	/**
	 * This method updates the weights using the gradient calculated using
	 * backPropagateSave().
	 * 
	 * @param batchSize the size of the batch used to calculate the gradient, it is
	 *                  used to average the gradient.
	 */
	public void updateWeights(int batchSize) {
		// We average the weights and bias gradient, and we subtract it (reducing the
		// cost function).
		outputLayer.weights = outputLayer.weights
				.subtract(outputLayerWeightGradiant.multiply(1.0 / batchSize).multiply(learningRate));
		outputLayer.bias = outputLayer.bias
				.subtract(outputLayerBiasGradiant.multiply(1.0 / batchSize).multiply(learningRate));
		// We do the same for all the hidden layers.
		for (int layerIndex = 0; layerIndex < hiddenLayers.length; layerIndex++) {
			MLPLayer layer = hiddenLayers[layerIndex];
			hiddenLayers[layerIndex].weights = layer.weights
					.subtract(hiddenLayersWeightGradiant[layerIndex].multiply(1.0 / batchSize).multiply(learningRate));
			hiddenLayers[layerIndex].bias = layer.bias
					.subtract(hiddenLayersBiasGradiant[layerIndex].multiply(1.0 / batchSize).multiply(learningRate));
		}
	}

	/**
	 * This method does the backpropagation and saves the gradients.
	 * 
	 * @param classification takes the classification of the data that lead to the
	 *                       output layer activation.
	 */
	public void backPropagateSave(int classification) {
		Matrix correctOutput = correctOutputMatrix(classification);
		// We calculate the error by subtracting the actual outcome (output layer
		// activation) from the expected outcome (correctOutput)
		Matrix derivativeCost = outputLayer.activation.subtract(correctOutput);
		// We then get the derivative of the activation of the output layer.
		Matrix derivativeActivation = outputLayer.weights.dotProduct(hiddenLayers[hiddenLayers.length - 1].activation);
		// We recalculate the activation of the outputLayer (without applying the
		// sigmoid)
		// And instead we apply the sigmoid derivative.
		derivativeActivation.applySigmoidDerivative();
		Matrix firstGradiant = derivativeCost.multiply(derivativeActivation);
		// We then use the dot product between them to get the gradient matrix for the
		// weights of the output layer.
		Matrix weightGradiant = firstGradiant.dotProduct(hiddenLayers[hiddenLayers.length - 1].activation.transpose());
		// We sum them up so we can average them later.
		this.outputLayerWeightGradiant = this.outputLayerWeightGradiant.sum(weightGradiant);
		this.outputLayerBiasGradiant = this.outputLayerBiasGradiant.sum(firstGradiant);
		// This loop backpropagates the error to the previous layers dynamically.
		for (int index = hiddenLayers.length - 1; index >= 0; index--) {
			// If we have reached the first hidden layer then the previous layer is the
			// input layer.
			MLPLayer layer = hiddenLayers[index];
			MLPLayer previousLayer = inputLayer;
			if (index > 0) {
				previousLayer = hiddenLayers[index - 1];
			}
			// We get the activation of the current layer using the previous layer and the
			// weights.
			derivativeActivation = layer.weights.dotProduct(previousLayer.activation);
			derivativeActivation.applySigmoidDerivative();
			// We apply the derivative of the sigmoid.
			MLPLayer frontLayer = outputLayer;
			if (index < hiddenLayers.length - 1) {
				frontLayer = hiddenLayers[index + 1];
			}
			// We get the transpose of the weights of the layer in front.
			Matrix transposedWeights = frontLayer.weights.transpose();
			firstGradiant = transposedWeights.dotProduct(firstGradiant).multiply(derivativeActivation);
			weightGradiant = firstGradiant.dotProduct(previousLayer.activation.transpose());
			// We use the dot product to get the gradient matrix.
			// We initialise the matrix if it has not been initialised yet.
			if (hiddenLayersWeightGradiant[index] == null) {
				hiddenLayersWeightGradiant[index] = new Matrix(weightGradiant.getNumRows(), weightGradiant.getNumCols());
				hiddenLayersBiasGradiant[index] = new Matrix(firstGradiant.getNumRows(), firstGradiant.getNumCols());
			}
			// We sum so we can average later.
			hiddenLayersBiasGradiant[index] = hiddenLayersBiasGradiant[index].sum(firstGradiant);
			hiddenLayersWeightGradiant[index] = hiddenLayersWeightGradiant[index].sum(weightGradiant);
		}
	}

	/**
	 * This method takes a classification and uses it to create a matrix resembling
	 * what the activation of the output layer should look like
	 * 
	 * @param classification
	 * @return a matrix containing a 1 in the position of the classification and a 0
	 *         everywhere else.
	 */
	public Matrix correctOutputMatrix(int classification) {
		// The initial value for a matrix is 0, so we don't need to set that up.
		Matrix result = new Matrix(numberClasses, 1);
		result.getData()[classification][0] = 1;
		return result;
	}

	/**
	 * This method calculates the cost of the MLP using its output layer activation
	 * values. The results is the sum of the squared difference of the expected
	 * perceptron activation with the actual. We then divide by 2.
	 * 
	 * @param classification
	 * @return a double value for the cost
	 */
	public double calculateCost(int classification) {
		double cost = 0;
		int index = 0;
		// We loop through the activation values of the output layer.
		for (double[] row : outputLayer.activation.getData()) {
			// When we reach the perceptron that corresponds to the classification we want
			// We use -1 as its expected outcome is 1. Evrything else is 0 as we expect them
			// to be 0.
			if (index++ == classification) {
				cost += Math.pow((row[0] - 1), 2);
			} else {
				cost += Math.pow((row[0] - 0), 2);
			}
		}
		return 0.5 * cost;
	}

	public double getAccuracy() {
		return accuracy;
	}

}
