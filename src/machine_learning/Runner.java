package machine_learning;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * This is the main class where execution starts, there are 3 algorithms implemented
 * for this problem. The first one is Euclidean distance with an average 98.2% accuracy,
 * the second one is Multi Layered Perceptron which achieved an accuracy of 95%, and the 
 * last one is SVM which achieves an average accuracy of 98.6%, but it can be pushed to 
 * 99% for one of the fold tests however the other one will not do as well.
 * Some parameters are fixed, but some can be changed to explore the differences in results.
 * 
 * @author Ayman El Behri
 *
 */

public class Runner {
	private static final String firstDatasetName = "cw2DataSet1.csv";
	private static final String secondDatasetName = "cw2DataSet2.csv";
	public static final String separator = ",";
	private static final int dataSize = 2810;
	private static final int singleInputSize = 65;
	// These parameters can be changed to explore their effects
	// layerParams is the structure of the hidden layers of the MLP this means there are 2 layers the first one
	// has 50 perceptrons and the second one has 40.
	private static final int[] layerParams = {50,40};
	private static final int numberClasses = 10;
	private static final double mlpLearningRate = 0.02, svmLearningRate = 0.001;
	private static final int mlpEpochs = 1000, svmEpochs = 1500;
	private static final int batchSize = 1;
	
	/**
	 * This is the main method which reads the data from the csv files, prepares the data by
	 * separating the labels and data, before feeding them to the algorithms which are trained 
	 * and tested. Examples of how to run the algorithms implemented.
	 * @param args
	 */
	public static void main(String[] args) {
		int[][] firstDataset = readData(firstDatasetName);
		int[][] secondDataset = readData(secondDatasetName);
		// Here we separate the labels from the data
		double[][] firstDatasetDouble = new double[firstDataset.length][singleInputSize-1];
		double[][] secondDatasetDouble = new double[secondDataset.length][singleInputSize-1];
		int[] firstLabels = new int[firstDataset.length];
		int[] secondLabels = new int[secondDataset.length];
		// We go through the data storing the labels separately from the actual data.
		for(int inputIndex = 0; inputIndex < firstDataset.length; inputIndex++) {
			int[] inputFirst = firstDataset[inputIndex];
			int[] inputSecond = secondDataset[inputIndex];
			// We save the labels (last value in the array).
			firstLabels[inputIndex] = inputFirst[singleInputSize-1];
			secondLabels[inputIndex] = inputSecond[singleInputSize-1];
			// The data is stored as double values so that they can be used by SVMs, and the MLP.
			double[] actualInputFirst = new double[singleInputSize-1];
			double[] actualInputSecond = new double[singleInputSize-1];
			for(int index = 0; index < singleInputSize-1; index++) {
				actualInputFirst[index] = inputFirst[index];
				actualInputSecond[index] = inputSecond[index];
			}
			firstDatasetDouble[inputIndex] = actualInputFirst;
			secondDatasetDouble[inputIndex] = actualInputSecond;
		}
		// This runs the Euclidean distance algorithm.
		
		
		EuclideanDistance firstModel = new EuclideanDistance(firstDataset, secondDataset);
		//firstModel.testAll();
		
		EuclideanDistance secondModel = new EuclideanDistance(secondDataset, firstDataset);
		//secondModel.testAll();
		
		
		// This is a two fold test of the MLP these following parameters give the highest accuracy I could find.
		
		
		MLP thirdModel = new MLP(singleInputSize-1, numberClasses, layerParams, mlpLearningRate, firstDatasetDouble, secondDatasetDouble, firstLabels, secondLabels);
		thirdModel.trainBatches(batchSize, mlpEpochs);
		
		MLP fourthModel = new MLP(singleInputSize-1, numberClasses, layerParams, mlpLearningRate, secondDatasetDouble, firstDatasetDouble, secondLabels, firstLabels);
		fourthModel.trainBatches(batchSize, mlpEpochs);
		
		
		// These two objects can classify digits using a 1v1 voting approach, to run the other multi class SVMs you can change the Class instantiated.
		
		
		MultiClassSVM classifier = new OtherVotingSVM(firstDatasetDouble, secondDatasetDouble, firstLabels, secondLabels, numberClasses, svmEpochs, svmLearningRate);
		classifier.train();
		classifier.testAll();
		
		MultiClassSVM secondClassifier = new OtherVotingSVM(secondDatasetDouble, firstDatasetDouble, secondLabels, firstLabels, numberClasses, svmEpochs, svmLearningRate);
		secondClassifier.train();
		secondClassifier.testAll();
		
		
	}
	
	/**
	 * This method reads data from a file and puts the data into a 2 dimensional array.
	 * @param fileName the file path that stores the data.
	 * @return the 2 dimensional integer array of data.
	 */
	public static int[][] readData(String fileName) {
		int[][] result = new int[dataSize][singleInputSize];
		try {
			Scanner scanner = new Scanner(new File(fileName));
			int resultIndex = 0;
			// We go through each line.
			while(scanner.hasNextLine()) {
				String[] splitLine = scanner.nextLine().split(",");
				// We split the data using the comma.
				int[] dataLine = new int[singleInputSize];
				for(int index = 0; index < splitLine.length; index++) {
					// We parse the data to an int, before putting it into the final array.
					dataLine[index] = Integer.parseInt(splitLine[index]);
				}
				result[resultIndex++] = dataLine;
			}
		}
		catch(FileNotFoundException ex) {
			System.out.println("File Not Found in the root folder " + ex.getMessage());
		}
		return result;
	}

}
