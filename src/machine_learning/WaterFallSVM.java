package machine_learning;

/**
 * This Class solves the multi class problem using SVMs, in a cascading water
 * fall method which goes down a list of SVMs until one of them classifies it as
 * a digit.
 * 
 * @author Ayman El Behri
 *
 */
public class WaterFallSVM extends MultiClassSVM {
	private SVM[] binarySVMs;
	private final int margCoef = 100;

	public WaterFallSVM(double[][] trainData, double[][] testData, int[] trainLabels, int[] testLabels,
			int numberClasses, int epochs, double learnRate) {
		super(trainData, testData, trainLabels, testLabels, numberClasses, epochs, learnRate);
		binarySVMs = new SVM[numberClasses];
		// We raise the dimensions.
		raiseDimensionsClosest(this.getTestData());
		raiseDimensionsClosest(this.getTrainData());
		// We will have 10 SVM classifiers.
		// Each SVM will separate one of the classes from each other class starting from 0
		// and separating it from every other class, and then going down until
		// separating 9 from every other class.
		for (int index = 0; index < numberClasses; index++) {
			int[] chosenClass = { index };
			binarySVMs[index] = new SVM(trainData, testData, trainLabels, testLabels, chosenClass, margCoef,
					this.getLearningRate(), 1);
		}

	}

	public void train() {
		testAll();
		for (SVM svm : binarySVMs) {
			svm.train(this.getEpochs());
		}
		testAll();
	}

	/**
	 * This method goes down the array of SVMs until we find a classification. We
	 * start from seeing if the input is a 0, if the binarySVMs[0] classifies the
	 * input as 0 then we return 0, otherwise we move to binarySVMs[1] if it
	 * classifies the input as 1 we return 1. We continue with this until we reach
	 * the end, if in the rare occasion that no SVM classifies it correctly, then we
	 * return a random class.
	 */
	public int classify(double[] input) {
		for (int index = 0; index < binarySVMs.length; index++) {
			SVM binarySVM = binarySVMs[index];
			if (binarySVM.classify(input) == binarySVM.getAboveIndice()) {
				return index;
			}
		}
		return (int) (Math.random() * 10);
	}

	public void analyseIndividualSVMs() {
		for (SVM svm : binarySVMs) {
			this.printSVMDetails(svm);
		}
	}

}
