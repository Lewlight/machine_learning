package machine_learning;

/**
 * This class represents an MLP layer. the activation is a matrix containing 1
 * column, and each row represents one pereceptron's activation. The weights is
 * a matrix where the number of rows is the number of perceptrons and the number
 * of columns is the size of the previous layer this way we can simply multiply
 * the weights matrix with the previous layers activation, and we end up with
 * the sum of weights*activation for each perceptron. The bias is a matrix
 * containing one column and each row corresponds to the bias of a perceptron.
 * 
 * @author Ayman El Behri
 *
 */
public class MLPLayer {
	public Matrix weights;
	public Matrix activation;
	public Matrix bias;
	private final int weightScale = 4;

	/**
	 * This constructor serves for the input layer as it doesn't have weights
	 * associated with it.
	 * 
	 * @param inputSize how many perceptrons in the layer.
	 */
	public MLPLayer(int inputSize) {
		activation = new Matrix(inputSize, 1);
	}

	/**
	 * This constructor is for hidden layers and the output layer.
	 * 
	 * @param inputSize the input size (should be the size of the previous layer)
	 * @param layerSize the size of the current layer
	 */
	public MLPLayer(int inputSize, int layerSize) {
		activation = new Matrix(layerSize, 1);
		bias = new Matrix(layerSize, 1);
		weights = new Matrix(layerSize, inputSize);
		// We randomise the weights using the weightScale.
		for (int rowIndex = 0; rowIndex < layerSize; rowIndex++) {
			for (int colIndex = 0; colIndex < inputSize; colIndex++) {
				weights.getData()[rowIndex][colIndex] = Math.random() * weightScale - weightScale*0.5;
			}
		}
	}

	/**
	 * This method calculates the new activation of this layer, using the previous
	 * layer. The result is simply using the dot product of the previous layers
	 * activation with the current layers weights, resulting in the sum of each
	 * weight multiplied with the perceptron's value. We then add the biases for
	 * each perceptron, and then we apply the activation funcion.
	 * 
	 * @param previousLayer
	 */
	public void feedPrevious(MLPLayer previousLayer) {
		activation = weights.dotProduct(previousLayer.activation);
		activation = activation.sum(bias);
		for (int index = 0; index < activation.getNumRows(); index++) {
			activation.getData()[index][0] = sigmoid(activation.getData()[index][0]);
		}
	}

	/**
	 * Calculates the sigmoid of a double value.
	 * 
	 * @param value
	 * @return The sigmoid value Between 0 and 1.
	 */
	public double sigmoid(double value) {
		return 1 / (1 + Math.exp(-value));
	}

	/**
	 * This is another sigmoid function.
	 * 
	 * @param value
	 * @return The sigmoid value returned is between -1 and 1.
	 */
	public double tanh(double value) {
		return ((Math.exp(2 * value) - 1) / (Math.exp(2 * value) + 1));
	}

	/**
	 * This calculates the derivative of tanh.
	 * 
	 * @param value
	 * @return The derivative value.
	 */
	public double tanhDerivative(double value) {
		return 1 - tanh(value) * tanh(value);
	}

}
