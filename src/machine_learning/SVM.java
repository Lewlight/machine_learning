package machine_learning;

import java.util.Arrays;

/**
 * This class represents a Support Vector Machine solved using gradient descent.
 * To solve this binary classification problem the constructor prepares the
 * labels changing it to a binary problem, and then it repeats for a certain
 * number of epochs constantly reducing the cost function, and adapting the
 * learning rate to reach a minimum for the cost function and maximise the
 * accuracy.
 * 
 * @author Ayman El Behri
 *
 */
public class SVM {

	private double[][] trainData, testData;
	private int[] trainLabels, testLabels;
	private int dataDimensions;
	private int[] classifications;
	private HyperPlane separator;
	private double marginCoef, learningRate;
	private Matrix positions;
	private double accuracy;
	private int aboveIndice;
	private final double reductionCoef = 0.2;
	// If necessary this field can be used to store the other classes the SVM is
	// separating from.
	public int[] otherClasses;

	/**
	 * The constructor prepares the data to be trained so that the HyperPlane is
	 * learned.
	 * 
	 * @param trainData
	 * @param testData
	 * @param trainLabels
	 * @param testLabels
	 * @param classifications A list of integers that we want to separate from every
	 *                        other class in the data.
	 * @param marginCoef      The margin coefficient which calibrates the margin
	 *                        higher values make the margin harder and smaller
	 *                        values make it softer.
	 * @param learningRate    The learning rate.
	 * @param above           An integer which should be 1 or -1 used to alter the
	 *                        labels so this problem can become a binary problem.
	 */
	public SVM(double[][] trainData, double[][] testData, int[] trainLabels, int[] testLabels, int[] classifications,
			double marginCoef, double learningRate, int above) {
		this.trainData = trainData;
		this.testData = testData;
		// We copy the labels so we can alter them.
		this.trainLabels = Arrays.copyOf(trainLabels, trainLabels.length);
		this.testLabels = Arrays.copyOf(testLabels, testLabels.length);
		this.dataDimensions = trainData[0].length;
		this.separator = new HyperPlane(dataDimensions, 16);
		this.marginCoef = marginCoef;
		this.learningRate = learningRate;
		this.classifications = classifications;
		this.aboveIndice = above;
		for (int index = 0; index < trainLabels.length; index++) {
			// We change the labels for the data chosen to be equal to the aboveIndice.
			if (linearSearch(this.classifications, trainLabels[index])) {
				this.trainLabels[index] = above;
			}
			// We change the other labels to the opposite label.
			else {
				this.trainLabels[index] = -1 * above;
			}
		}
		// We do the same for test data.
		for (int index = 0; index < testLabels.length; index++) {
			if (linearSearch(this.classifications, testLabels[index])) {
				this.testLabels[index] = above;
			} else {
				this.testLabels[index] = -1 * above;
			}
		}

	}

	/**
	 * This method calculates the cost of the SVM which we need to reduce.
	 * 
	 * @return a value for the cost.
	 */
	public double calculateCost() {
		double sum = 0;
		// We start by calculating the hinge loss representing how many
		// points are misclassified.
		// We get the dot product of the training data and the weights.
		Matrix product = new Matrix(trainData).dotProduct(separator.weights);
		for (int index = 0; index < product.getNumRows(); index++) {
			// We add the bias to each product before multiplying by the label of the point.
			product.getData()[index][0] += separator.bias;
			product.getData()[index][0] = 1 - product.getData()[index][0] * trainLabels[index];
		}
		// We then sum each row's value.
		for (double[] row : product.getData()) {
			// We only sum the values above 0.
			if (row[0] < 0) {
				row[0] = 0;
			}
			sum += row[0];
		}
		// We save the product since we need it to calculate the gradient.
		positions = product;
		// We multiply the sum with the margin coefficient and averaging it.
		sum = marginCoef * sum / trainData.length;
		// We then get the scalar value of the weights before summing them to get the
		// cost.
		double weightScalar = 0;
		for (double[] row : separator.weights.getData()) {
			weightScalar += row[0] * row[0];
		}
		weightScalar = 0.5 * Math.sqrt(weightScalar);
		return weightScalar + sum;
	}

	/**
	 * This method calculates the gradient used to update the weights.
	 * 
	 * @return a matrix used to update the weights.
	 */
	public Matrix calculateWeightGradient() {
		Matrix result = new Matrix(separator.weights.getNumRows(), separator.weights.getNumCols());

		for (int index = 0; index < positions.getNumRows(); index++) {
			// If the hinge loss is 0 the gradient is the matrix of weights itself.
			// In this case we only maximise the "margin".
			double position = positions.getData()[index][0];
			if (position <= 0) {
				result = result.sum(separator.weights);
			}
			// If the hinge loss is not 0 we adjust the weights so that it tries to
			// correctly classify the point which leads to smaller hinge loss.
			// We also incorporate the weights as part of the gradient to subsequently
			// maximise the margin.
			else {
				Matrix multiplied = new Matrix(trainData[index]).multiply(trainLabels[index]);
				result = result.sum(separator.weights.subtract(multiplied.multiply(this.marginCoef)));
			}
		}
		// We calculate the average at the end.
		return result.multiply(1.0 / positions.getNumRows());
	}

	/**
	 * This method calculates the bias gradient.
	 * 
	 * @return the value of the negative of the gradient.
	 */
	public double calculateBiasGradient() {
		double result = 0;
		for (int index = 0; index < trainData.length; index++) {
			// If the hinge loss is not 0 then we add to the gradient.
			// The gradient is equal to the label multiplied by the margin coefficient.
			if (!(positions.getData()[index][0] <= 0)) {
				result = trainLabels[index] * this.marginCoef;
				separator.bias += result;
			}
		}
		return result / trainData.length;
	}

	/**
	 * This method classifies a point in the space.
	 * 
	 * @param input the data we want to classify.
	 * @return 1 if it's above the plane, -1 if below and 0 if it's on the plane.
	 */
	public int classify(double[] input) {
		Matrix matrixInput = new Matrix(input).transpose();
		return separator.checkIfAbove(matrixInput);
	}

	/**
	 * This method trains the SVM using gradient descent.
	 * 
	 * @param epochs
	 */
	public void train(int epochs) {
		// We assume that the initial cost is lower than the old cost.
		double cost = 0;
		double oldCost = Double.MAX_VALUE;
		int oldIter = 0;
		int iter = 0;
		while (iter < epochs) {
			iter++;
			// If we go above 5000 iterations we half the learning rate.
			// As it is highly probable that we are nearing the minimum.
			if (iter - oldIter == 5000) {
				System.out.println("The cost diff is " + Math.abs(cost - oldCost));
				learningRate = learningRate / 2;

			}
			// We calculate the cost.
			cost = calculateCost();
			// We have a check to reduce the learning rate in case the cost starts
			// oscillating.
			// This happens if the oldCost is larger than the current cost.
			if (cost > oldCost + cost * this.reductionCoef) {
				System.out.println("dividng the learning rate " + learningRate / 2);
				learningRate = learningRate / 2;
			}

			// We update the weights and the bias.
			Matrix gradient = calculateWeightGradient();
			separator.weights = separator.weights.subtract(gradient.multiply(learningRate));

			separator.bias += calculateBiasGradient() * learningRate;

			oldCost = cost;
		}
		// And we test using the test data and training data.
		testAll();
		testTrainAll();
	}

	/**
	 * This method tests the HyperPlane separator accuracy.
	 * 
	 * @return the accuracy of the SVM.
	 */
	public double testAll() {
		int correctClassifications = 0;
		// We go through the test data.
		for (int index = 0; index < testData.length; index++) {
			double[] input = testData[index];
			int classification = classify(input);
			// if the classification is equal to the label we add 1.
			if (classification == testLabels[index]) {
				correctClassifications++;
			}
		}
		accuracy = (double) correctClassifications / testData.length;
		// We print out some relevant Information.
		System.out.println("The accuracy is " + accuracy);
		System.out.println("The correct classifications " + correctClassifications);
		System.out.println("The incorrect classifications " + (testData.length - correctClassifications));
		System.out.println("The classes we want to separate frm the rest: " + Arrays.toString(this.classifications));

		return accuracy;
	}

	/**
	 * This method tests the SVM using the training data, which is useful when
	 * trying to analyse the results. It works in the same way as the testAll()
	 * except we use the training data instead.
	 * 
	 * @return the accuracy of the SVM on the training data.
	 */
	public double testTrainAll() {
		int correctClassifications = 0;
		for (int index = 0; index < trainData.length; index++) {
			double[] input = trainData[index];
			int classification = classify(input);
			if (classification == trainLabels[index]) {
				correctClassifications++;
			}
		}
		double trainAccuracy = (double) correctClassifications / trainData.length;
		System.out.println("--------------------------------Trainign:");
		System.out.println("The accuracy is " + trainAccuracy);
		System.out.println("The correct classifications " + correctClassifications);
		System.out.println("The incorrect classifications " + (trainData.length - correctClassifications));
		System.out.println("The classes we want to separate frm the rest: " + Arrays.toString(this.classifications));
		return trainAccuracy;
	}

	/**
	 * This method conducts a linear search in an array for some value.
	 * 
	 * @param array
	 * @param value
	 * @return true if the int exists in the array, return false otherwise.
	 */
	public boolean linearSearch(int[] array, int value) {
		for (int otherValue : array) {
			if (otherValue == value)
				return true;
		}
		return false;
	}

	public int[] getClassifications() {
		return classifications;
	}

	public double getMarginCoef() {
		return marginCoef;
	}

	public double getAccuracy() {
		return accuracy;
	}

	public int getAboveIndice() {
		return aboveIndice;
	}

}