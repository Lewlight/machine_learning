package machine_learning;

/**
 * This class produced the highest accuracy amongst all the methods implemented
 * here. It uses a one-vs-one voting system to classify different inputs. Each
 * SVM used tries to classify one class digit for example 9 as opposed to
 * another digit for example 1. The class with the highest votes is the class
 * predicted.
 * 
 * @author Ayman El Behri
 *
 */
public class OtherVotingSVM extends MultiClassSVM {
	private SVM[] binarySVMs;
	// These coefficients were fine tuned to maximise the accuracy.
	// By continuously testing and choosing the margin coefficients with the highest
	// accuracy. Each row corresponds to a digit and each column corresponds to the digit
	// it is being compared to. (row 0 and column 1, is the margin coefficient used
	// for the SVM that splits zeros and twos.
	private final double[][] coefficients = { { 5.0, 1.0, 5.0, 0.7, 1.5, 1.0, 5.0, 5.0, 1.0, },
			{ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.9, 1.0, }, { 1.0, 1.0, 5.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, },
			{ 1.0, 1.0, 5.0, 1.0, 5.0, 1.0, 1.0, 1.0, 5.0, }, { 0.7, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 10.0, 1.0, },
			{ 1.5, 1.0, 1.0, 5.0, 1.0, 1.0, 5.0, 0.7, 0.7, }, { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, },
			{ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, }, { 5.0, 1.0, 1.0, 0.9, 10.0, 0.7, 1.0, 1.0, 5.0, },
			{ 1.0, 1.0, 2.0, 5.0, 1.0, 0.7, 5.0, 1.0, 5.0, }, };

	/**
	 * This constructor creates the instances of SVM which will be used for the
	 * final classification. Each SVM is a 1vs1 classification between each digit
	 * and another digit.
	 * 
	 * @param trainData
	 * @param testData
	 * @param trainLabels
	 * @param testLabels
	 * @param numberClasses
	 * @param epochs
	 * @param learnRate
	 */
	public OtherVotingSVM(double[][] trainData, double[][] testData, int[] trainLabels, int[] testLabels,
			int numberClasses, int epochs, double learnRate) {
		super(trainData, testData, trainLabels, testLabels, numberClasses, epochs, learnRate);

		// We start by raising the dimensions from 64 to 74.
		raiseDimensionsClosest(this.getTestData());
		raiseDimensionsClosest(this.getTrainData());
		binarySVMs = new SVM[(getNumberClasses() * (getNumberClasses() - 1)) / 2];
		int iterator = 0;
		// We loop through the classes creating the needed 1v1 SVMs. (for our case we
		// end up with 45 different SVMs).
		for (int classIndex = 0; classIndex < getNumberClasses(); classIndex++) {
			for (int otherClassIndex = classIndex + 1; otherClassIndex < getNumberClasses(); otherClassIndex++) {
				// We create the array of the classes which will be separated with an SVM.
				int[] classesChosen = { classIndex, otherClassIndex };
				// We cut the data to keep only the data belonging to either classes.
				double[][] splitTraining = cutData(getTrainData(), getTrainLabels(), classesChosen);
				int[] splitTrainingLabels = cutLabels(getTrainLabels(), classesChosen);
				double[][] splitTest = cutData(getTestData(), testLabels, classesChosen);
				int[] splitTestLabels = cutLabels(getTestLabels(), classesChosen);
				int[] chosenClass = { classIndex };
				// We use the coefficients array to get the margin coefficients with the best
				// results.
				SVM svm = new SVM(splitTraining, splitTest, splitTrainingLabels, splitTestLabels, chosenClass,
						coefficients[classIndex][otherClassIndex - 1], getLearningRate(), 1);
				int[] otherClass = { otherClassIndex };
				// We set the otherClass Field so it can be used when picking up the votes.
				svm.otherClasses = otherClass;
				binarySVMs[iterator++] = svm;
			}
		}
	}

	public void train() {
		for (SVM svm : binarySVMs) {
			svm.train(getEpochs());
		}
	}

	/**
	 * This method classifies the data input, by going through all of the data and
	 * gathering votes for each class, the class with the highest number of votes is
	 * returned.
	 * 
	 * @return The class predicted with the highest votes.
	 */
	public int classify(double[] input) {
		int[] votes = new int[getNumberClasses()];
		// We first collect all of the votes.
		// We loop through the array of SVMs.
		for (SVM svm : binarySVMs) {
			// The predicted class found by the SVM is where the vote is cast.
			if (svm.classify(input) == svm.getAboveIndice()) {
				// svm.classifications[0] is class above the hyper plane
				// classified as aboveIndice.
				votes[svm.getClassifications()[0]]++;
			} else {
				// svm.otherClasses[0] is the other class used by the SVM.
				votes[svm.otherClasses[0]]++;
			}
		}
		// Find the highest vote by linearly searching through the votes.
		int highestClass = 0;
		int highestVote = -1;
		for (int index = 0; index < getNumberClasses(); index++) {
			if (votes[index] >= highestVote) {
				highestVote = votes[index];
				highestClass = index;
			}
		}
		return highestClass;

	}

	/**
	 * This method prints out the details of the SVMs used to classify data.
	 */
	public void analyseIndividualSVMs() {
		for (SVM svm : binarySVMs) {
			this.printSVMDetails(svm);
		}
	}
}
