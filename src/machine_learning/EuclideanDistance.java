package machine_learning;

/**
 * This class classifies digits using the Euclidean distance. It finds the
 * closest point from the training data and classifies it as that point.
 * 
 * @author Ayman El Behri
 *
 */
public class EuclideanDistance {
	private final int[][] trainDataset;
	private final int[][] testDataset;
	private final int classIndex;

	public EuclideanDistance(int[][] trainingSet, int[][] testingSet) {
		this.trainDataset = trainingSet;
		this.testDataset = testingSet;
		this.classIndex = testDataset[0].length - 1;
	}

	/**
	 * This method looks through the training data to find the closest point, and
	 * the classification returned is the label of the closest point.
	 * 
	 * @param input a data point.
	 * @return The classification of the data point.
	 */
	public int classify(int[] input) {
		// we keep track of the closest point and its distance.
		int prediction = trainDataset[0][classIndex];
		double smallestDistance = distance(input, trainDataset[0]);
		// we search through all the points to find the closest point.
		for (int index = 1; index < this.trainDataset.length; index++) {
			double distanceToPoint = distance(input, trainDataset[index]);
			if (distanceToPoint <= smallestDistance) {
				smallestDistance = distanceToPoint;
				prediction = trainDataset[index][classIndex];
			}
		}
		return prediction;
	}

	/**
	 * This method classifies all of the test data and prints out relevant
	 * information such as the accuracy the number of correct and incorrect
	 * predictions, and the accuracy of each digit class.
	 */
	public void testAll() {
		// This keeps track of the mistakes the classifier makes for each digit.
		int[] correctClassAccuracy = new int[10];
		int[] incorrectClassAccuracy = new int[10];
		int correctPredictions = 0;
		int wrongPredictions = 0;
		// This loops through the test data set.
		for (int[] testInput : testDataset) {
			int prediction = classify(testInput);
			// We compare the predicted class with the label
			// The label for each data point is the last digit in array.
			if (prediction == testInput[classIndex]) {
				correctPredictions++;
				correctClassAccuracy[testInput[classIndex]]++;
			} else {
				wrongPredictions++;
				incorrectClassAccuracy[testInput[classIndex]]++;
			}
		}
		// We then print the results.
		System.out.println("The number of correct predictions: " + correctPredictions);
		System.out.println("The number of wrong predictions: " + wrongPredictions);
		System.out.println("The accuracy of this model: " + (double) correctPredictions / testDataset.length);
	}

	/**
	 * This calculates the distance between two points from the data
	 * 
	 * @param firstPoint
	 * @param secondPoint
	 * @return the distance between the two points.
	 */
	public double distance(int[] firstPoint, int[] secondPoint) {
		double sum = 0;
		// We don't take into consideration the last digit since that's the label.
		for (int index = 0; index < firstPoint.length - 1; index++) {
			sum += Math.pow((firstPoint[index] - secondPoint[index]), 2);
		}
		return Math.sqrt(sum);
	}

}
