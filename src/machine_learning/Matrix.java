package machine_learning;

import java.util.Arrays;

/**
 * 
 * @author Ayman El Behri This class is a 2 dimensional Matrix with the
 *         operations necessary for the algorithms implemented. The linear
 *         operations necessary are dot product, sum, subtraction, scalar
 *         product, transposing and applying a sigmoid function to all the
 *         values. No checks are done in here, however a better implementation
 *         will check the number of rows and columns before The methods execute.
 */
public class Matrix {
	private int numRows;
	private int numCols;
	private double[][] data;

	/**
	 * A constructor that takes a 1 dimensional input and puts it into a one column
	 * matrix.
	 * 
	 * @param input Data used in the one column matrix.
	 */
	public Matrix(double[] input) {
		numRows = input.length;
		numCols = 1;
		data = new double[numRows][numCols];
		int index = 0;
		for (double[] row : data) {
			row[0] = input[index++];
		}
	}

	/**
	 * A Constructor that takes a 2 dimensional input and places it into a Matrix
	 * object.
	 * 
	 * @param input 2 dimensional array.
	 */
	public Matrix(double[][] input) {
		numRows = input.length;
		numCols = input[0].length;
		data = input;
	}

	/**
	 * A constructor created with the specified values for the 2 dimensions, and
	 * creates a matrix of zeros.
	 * 
	 * @param numRows an integer representing the number of rows needed.
	 * @param numCols an integer representing the number of columns needed.
	 */
	public Matrix(int numRows, int numCols) {
		this.numRows = numRows;
		this.numCols = numCols;
		data = new double[numRows][numCols];
	}

	/**
	 * This method returns the dot product of two matrices.
	 * 
	 * @param other Another matrix to do the dot Product with.
	 * @return The result of the dot product.
	 */
	public Matrix dotProduct(Matrix other) {
		Matrix result = new Matrix(this.numRows, other.numCols);
		for (int rowIndex = 0; rowIndex < this.numRows; rowIndex++) {
			double[] row = this.data[rowIndex];
			for (int colIndex = 0; colIndex < other.numCols; colIndex++) {
				double value = 0;
				for (int index = 0; index < row.length; index++) {
					value += row[index] * other.data[index][colIndex];
				}
				result.data[rowIndex][colIndex] = value;
			}
		}
		return result;
	}

	/**
	 * This method sums two matrices by adding each cell's values.
	 * 
	 * @param other The other matrix to sum up.
	 * @return The result matrix with the same dimensions as the components.
	 */
	public Matrix sum(Matrix other) {
		Matrix result = new Matrix(this.numRows, this.numCols);
		for (int rowIndex = 0; rowIndex < this.numRows; rowIndex++) {
			for (int colIndex = 0; colIndex < this.numCols; colIndex++) {
				result.data[rowIndex][colIndex] = this.data[rowIndex][colIndex] + other.data[rowIndex][colIndex];
			}
		}
		return result;
	}

	/**
	 * This method subtracts two matrices by subtracting each cell's values.
	 * 
	 * @param other The other matrix to subtract.
	 * @return The result matrix with the same dimensions as the components.
	 */
	public Matrix subtract(Matrix other) {
		Matrix result = new Matrix(this.numRows, this.numCols);
		for (int rowIndex = 0; rowIndex < this.numRows; rowIndex++) {
			for (int colIndex = 0; colIndex < this.numCols; colIndex++) {
				result.data[rowIndex][colIndex] = this.data[rowIndex][colIndex] - other.data[rowIndex][colIndex];
			}
		}
		return result;
	}

	/**
	 * This method does a component wise multiplication of two matrices.
	 * 
	 * @param other The other matrix to multiply.
	 * @return The result matrix with the same dimension values as the components.
	 */
	public Matrix multiply(Matrix other) {
		if (other.numCols != numCols || other.numRows != numRows) {
			System.out.println("This multiplication is flawed ");
		}
		Matrix result = new Matrix(numRows, numCols);
		for (int rowIndex = 0; rowIndex < numRows; rowIndex++) {
			for (int colIndex = 0; colIndex < numCols; colIndex++) {
				result.data[rowIndex][colIndex] = data[rowIndex][colIndex] * other.data[rowIndex][colIndex];
			}
		}
		return result;
	}

	/**
	 * This method multiplies each element of the matrix with a certain value.
	 * 
	 * @param value a double value to multiply each cell's value with.
	 * @return A new Matrix of the calculated values.
	 */
	public Matrix multiply(double value) {
		Matrix result = new Matrix(numRows, numCols);
		for (int rowIndex = 0; rowIndex < numRows; rowIndex++) {
			for (int colIndex = 0; colIndex < numCols; colIndex++) {
				result.data[rowIndex][colIndex] = data[rowIndex][colIndex] * value;
			}
		}
		return result;
	}

	/**
	 * This method applies a sigmoid method to all the values of the matrix.
	 */
	public void applySigmoid() {
		for (int rowIndex = 0; rowIndex < this.numRows; rowIndex++) {
			for (int colIndex = 0; colIndex < this.numCols; colIndex++) {
				this.data[rowIndex][colIndex] = sigmoid(this.data[rowIndex][colIndex]);
			}
		}
	}

	/**
	 * This method applies the method tanh to all the values of the matrix.
	 */
	public void applyTanh() {
		for (int rowIndex = 0; rowIndex < this.numRows; rowIndex++) {
			for (int colIndex = 0; colIndex < this.numCols; colIndex++) {
				this.data[rowIndex][colIndex] = tanh(this.data[rowIndex][colIndex]);
			}
		}
	}

	/**
	 * This method applies the derivative of tanh to all the values of the matrix.
	 */
	public void applyTanhDerivative() {
		for (int rowIndex = 0; rowIndex < this.numRows; rowIndex++) {
			for (int colIndex = 0; colIndex < this.numCols; colIndex++) {
				this.data[rowIndex][colIndex] = tanhDerivative(this.data[rowIndex][colIndex]);
			}
		}
	}

	/**
	 * This method applies the derivative of the same sigmoid function to all the
	 * values.
	 */
	public void applySigmoidDerivative() {
		for (int rowIndex = 0; rowIndex < this.numRows; rowIndex++) {
			for (int colIndex = 0; colIndex < this.numCols; colIndex++) {
				this.data[rowIndex][colIndex] = sigmoid(this.data[rowIndex][colIndex])
						* (1 - sigmoid(this.data[rowIndex][colIndex]));
			}
		}
	}

	/**
	 * @return The transpose of the matrix.
	 */
	public Matrix transpose() {
		Matrix result = new Matrix(numCols, numRows);
		for (int rowIndex = 0; rowIndex < numRows; rowIndex++) {
			for (int colIndex = 0; colIndex < numCols; colIndex++) {
				result.data[colIndex][rowIndex] = this.data[rowIndex][colIndex];
			}
		}
		return result;
	}

	/**
	 * Calculates the sigmoid of a double value.
	 * 
	 * @param value
	 * @return The sigmoid value Between 0 and 1.
	 */
	public double sigmoid(double value) {
		return 1 / (1 + Math.exp(-value));
	}

	/**
	 * This is another sigmoid function.
	 * 
	 * @param value
	 * @return The sigmoid value returned is between -1 and 1.
	 */
	public double tanh(double value) {
		return ((Math.exp(2 * value) - 1) / (Math.exp(2 * value) + 1));
	}

	/**
	 * This calculates the derivative of tanh.
	 * 
	 * @param value
	 * @return The derivative value.
	 */
	public double tanhDerivative(double value) {
		return 1 - tanh(value) * tanh(value);
	}

	public String toString() {
		String result = "The dimensions are: " + numRows + " " + numCols;
		for (double[] row : data) {
			result += "\n " + Arrays.toString(row);
		}
		return result;
	}

	public int getNumRows() {
		return numRows;
	}

	public int getNumCols() {
		return numCols;
	}

	public double[][] getData() {
		return data;
	}
	
}
